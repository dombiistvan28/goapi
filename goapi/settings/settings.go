package settings

import (
	"database/sql"
)

const (
	TokenLiveMinutes        = 30;
	DbDriver         string = "mysql";
	Host             string = "localhost";
	Salt             string = "EhmOWtH03e2V808rCBRY";
	SqlInsertTemp    string = "INSERT INTO `%s` (%s) VALUES (%s)";
	SqlUpdateTemp    string = "UPDATE `%s` SET %s WHERE %s";
	//SELECT {columns} FROM {table} WHERE {conditons} {additionals}
	//{additionals} will be replaced to {groupby}&|{having}&|{order}&|{limit} if necessary
	SqlSelectTemp       string = "SELECT %s FROM `%s` WHERE %s %s";
	Secret              string = "472e9d8129df8bda5dec7e2f49ee8f8d";
	EntityLimit                = 10;
	LimitParam          string = "page";
	AuthenticationError string = "Authentication Error";
)

var (
	JsonDbConf struct {
		DbHost    string
		DbPort    string
		DbUser    string
		DbPass    string
		DbName    string
		CharSet   string
		DbCollate string
	}
	Connection *sql.DB
	BasePath string
);