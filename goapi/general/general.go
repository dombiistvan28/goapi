package general

import (
	"database/sql"
	"goapi/settings"
	"fmt"
	"net/http"
	"encoding/json"
	"strconv"
)

//run query, check error, return result
func ExecSql(query string) sql.Result {
	res, err := settings.Connection.Exec(query);
	CheckError(err);
	return res;
}

//get limit string to sql from page and limit
func GetLimitStr(page int, limit int) string {
	if (limit == 0) {
		return "";
	}
	if (page < 1) {
		page = 1;
	}
	limitFrom := (page - 1) * limit;
	limitStr := fmt.Sprintf("LIMIT %v,%v", limitFrom, limit);
	return limitStr;
}

//send response
func SendResponse(w http.ResponseWriter, code int, object interface{}) {
	w.WriteHeader(code);
	respJson, _ := json.Marshal(object);
	w.Write(respJson);
}

//get Header parameter
func GetHeaderParam(r *http.Request, parameter string) string {
	return r.Header.Get(parameter);
}

//get url parameter and convert to int
func GetUrlParamInt(r *http.Request, parameter string) int {
	val, _ := strconv.Atoi(r.URL.Query().Get(parameter));
	return val;
}

//check if err is nil, and panic if not
func CheckError(err error) {
	if (err != nil) {
		panic(err);
	}
}