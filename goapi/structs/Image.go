package structs

import (
	"net/http"
	"strings"
	"os"
	"io"
	"mime/multipart"
	"bytes"
	"goapi/settings"
)

//image type to validate
type Image struct {
	Input     string
	File      multipart.File
	Mime      string `validate:"nonzero,mustContainImage"`
	Size      int64  `validate:"max=5242880"`
	Name      string `validate:"nonzero"`
	Extension string `validate:"imageExtension"`
	Header    *multipart.FileHeader
}

//init image from formrequest, set mime type, filename and content
func (i *Image) Init(r *http.Request) (bool, error) {
	var err error
	i.File, i.Header, err = r.FormFile(i.Input);
	defer i.File.Close();
	if (nil == i.File) {
		return false, err;
	}

	var buffer bytes.Buffer
	fileSize, err := buffer.ReadFrom(i.File)

	if (err != nil) {
		return false, err;
	}
	contentType := http.DetectContentType(buffer.Bytes())
	i.Mime = contentType;
	explName := strings.Split(i.Header.Filename, ".");
	i.Extension = explName[len(explName)-1];
	i.Name = strings.Replace(i.Header.Filename, "."+i.Extension, "", 1);
	i.Extension = explName[len(explName)-1];
	i.Size = fileSize;
	return true, nil;
}

//upload image to passed path on the given filename (or the original one)
func (i *Image) Upload(r *http.Request, path string, filename string) (bool, string, error) {
	passedPath := strings.Trim(path, "/");
	path = settings.BasePath + "/" + passedPath;
	if _, err := os.Stat(path); os.IsNotExist(err) {
		err = os.MkdirAll(path, 0666);
	}

	if ("" == filename) {
		filename = i.Name + "." + i.Extension;
	}

	file, _, err := r.FormFile(i.Input) // img is the key of the form-data

	if (nil != err) {
		return false, "", err;
	}

	defer file.Close()

	f, err := os.OpenFile(path+"/"+filename, os.O_WRONLY|os.O_CREATE, 0666)
	if err != nil {
		return false, "", err;
	}
	defer f.Close()
	io.Copy(f, file)

	return true, passedPath + "/" + filename, nil;
}