package structs

import (
	"mime/multipart"
	"github.com/dgrijalva/jwt-go"
	"net/http"
	"goapi/general"
	"gopkg.in/validator.v2"
	"reflect"
)

//validate object
func Validate(vo interface{}, w http.ResponseWriter, contentType string) bool{
	err := validator.Validate(vo);
	if (err != nil) {
		status := http.StatusBadRequest;

		if(reflect.TypeOf(vo).String() == "structs.AuthorizedRequest"){
			status = http.StatusForbidden;
		}

		general.SendResponse(w, status, ErrorResponse{err.Error()});
		return false;
	}

	return true;
}

//validating token request
type TokenRequest struct {
	Email    string `validate:"nonzero,email"`
	Password string `validate:"nonzero"`
}

func (r TokenRequest) Validate(w http.ResponseWriter) bool {
	return Validate(r,w,"");
}

//error response
type ErrorResponse struct {
	Error string `json:"error"`
}

//every authorization required request implement this struct
type AuthorizedRequest struct {
	Token string `validate:"nonzero,validToken"`
}

func (r AuthorizedRequest) Validate(w http.ResponseWriter) bool {
	return Validate(r,w,"");
}

//book list request to validate
type BookListRequest struct {
}

//get book entity request to validate
type GetBookEntityRequest struct {
	BookId int64 `validate:"min=1,nonzero"`
}

func (r GetBookEntityRequest) Validate(w http.ResponseWriter) bool {
	return Validate(r,w,"");
}

//add book entity request to validate
type InsertBookEntityRequest struct {
	Title   string    `json:"title",validate:"nonzero"`
	Author  string    `json:"author",validate:"nonzero"`
	Comment []Comment `json:"comment"`
}

func (r InsertBookEntityRequest) Validate(w http.ResponseWriter) bool {
	return Validate(r,w,"");
}

//add book entity cover request to validate
type InsertBookCoverRequest struct {
	File multipart.File
}

func (r InsertBookCoverRequest) Validate(w http.ResponseWriter) bool {
	return Validate(r,w,"");
}

//get book cover request to validate
type GetBookCoverRequest struct {
}

func (r GetBookCoverRequest) Validate(w http.ResponseWriter) bool {
	return Validate(r,w,"");
}

//update book entity request to validate
type UpdateBookEntityRequest struct {
	Id      int64     `json:"book_id"`
	Title   string    `json:"title",validate:"nonzero"`
	Author  string    `json:"author",validate:"nonzero"`
	Comment []Comment `json:"comment"`
}

func (r UpdateBookEntityRequest) Validate(w http.ResponseWriter) bool {
	return Validate(r,w,"");
}

//book entity response
type BookEntityResponse struct {
	Book Book `json:"book"`
}

//book list response
type BookListResponse struct {
	Book []Book `json:"book"`
}

//book insert/update response
type SuccessModifyResponse struct {
	Book Book `json:"book"`
}

//claims to jwt struct
type Claims struct {
	User User `json:"user"`
	// recommended having
	jwt.StandardClaims
}

//token response
type TokenResponse struct {
	Token string `json:"token"`
}
