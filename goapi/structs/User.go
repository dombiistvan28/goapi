package structs

import (
	"fmt"
	"crypto/md5"
	"encoding/hex"
	"database/sql"
	"time"
	"github.com/dgrijalva/jwt-go"
	"goapi/settings"
	"goapi/general"
)

// User entity
type User struct {
	Id               int64
	Email            string
	Password         string
	Token            string
	Token_expires_at string
}

// Save User to Database, insert or update: depends on has ID or no
func (u *User) Save() {
	if (u.Id == 0) {
		sqlTemp := fmt.Sprintf(settings.SqlInsertTemp, "user", "`email`,`password`,`token`,`token_expires_at`", "?,?,?,?");
		stmt, err := settings.Connection.Prepare(sqlTemp);
		general.CheckError(err);
		res, err := stmt.Exec(u.Email, u.Password, u.Token, u.Token_expires_at);
		general.CheckError(err);
		u.Id, err = res.LastInsertId();
		general.CheckError(err);
	} else {
		sqlTemp := fmt.Sprintf(settings.SqlUpdateTemp, "user", "`email` = ?, `password` = ?, `token` = ?, `token_expires_at` = ?", "`id` = ?");
		stmt, err := settings.Connection.Prepare(sqlTemp);
		general.CheckError(err);
		_, err = stmt.Exec(u.Email, u.Password, u.Token, u.Token_expires_at, u.Id);
		general.CheckError(err);
	}
}

// Validate user by given password. The user has to be prepeared by loadByEmail before it is called
func (user User) ValidateByPassword(p string) bool {
	hasher := md5.New()
	hasher.Write([]byte(settings.Salt + p + settings.Salt))
	hash := hex.EncodeToString(hasher.Sum(nil));
	return user.Password == hash;
}

//set hashed password to user. Has to be passed by simple string fe.: u.setPassword("Newpassword1234")
func (user *User) SetPassword(p string) {
	hasher := md5.New()
	hasher.Write([]byte(settings.Salt + p + settings.Salt))
	hash := hex.EncodeToString(hasher.Sum(nil));
	user.Password = hash;
}

//load User by Email and Password return it
func (u User) LoadEmailPass(email string, password string) User {
	sqlTemp := fmt.Sprintf(settings.SqlSelectTemp, "*", "user", "`email` = ?", "");
	row := settings.Connection.QueryRow(sqlTemp, email)
	err := row.Scan(&u.Id, &u.Email, &u.Password, &u.Token, &u.Token_expires_at);
	if err == sql.ErrNoRows {
		return User{};
	} else {
		validUserRequest := u.Id > 0;
		if (validUserRequest && u.ValidateByPassword(password)) {
			return u;
		} else {
			return User{};
		}
	}

	return u;
}

//create accesstoken if the user hasn't get and returns it
func (u *User) GetAccessToken() string {
	now := time.Now();
	if (len(u.Token) > 0 && u.Token_expires_at > now.Format("2006-01-02 15:04:05")) {
		signedToken := u.Token;
		return signedToken;
	} else {
		expireToken := now.Add(settings.TokenLiveMinutes * time.Minute);
		claims := Claims{
			User{u.Id, u.Email, "", "", ""},
			jwt.StandardClaims{
				ExpiresAt: expireToken.Unix(),
				Issuer:    fmt.Sprintf("%s:8090", settings.Host),
			},
		}

		// Create the token using your claims
		token := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)

		// Signs the token with a secret.
		signedToken, _ := token.SignedString([]byte(settings.Secret));
		u.Token = signedToken;
		u.Token_expires_at = expireToken.Format("2006-01-02 15:04:05");
		u.Save();
		return signedToken;
	}
}

//generate test user
func (u User) GenerateTestUser() {
	newUser := User{0, "test@mail.com", "", "", ""}
	newUser.SetPassword("password1234");
	newUser.Save();
}
