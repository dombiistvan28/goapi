package structs

import (
	"fmt"
	"goapi/settings"
	"database/sql"
	"goapi/general"
	"errors"
	"gopkg.in/validator.v2"
)

//comment entity struct
type Comment struct {
	Id      int64  `json:"id",validate:"min=1"`
	Name    string `json:"name",validate:"nonzero"`
	Comment string `json:"comment",validate:"nonzero"`
	BookId  int64  `json:"book_id"`
}

//save Comment to database
func (c *Comment) Save() (bool, error) {
	if errs := validator.Validate(c); errs != nil {
		return false, errors.New("The comment contains inappropriate properties.");
	}
	if (c.Id == 0) {
		sqlTemp := fmt.Sprintf(settings.SqlInsertTemp, "comment", "`name`,`comment`,`book_id`", "?,?,?");
		stmt, _ := settings.Connection.Prepare(sqlTemp);
		res, _ := stmt.Exec(c.Name, c.Comment, c.BookId);
		c.Id, _ = res.LastInsertId();
		return c.Id > 0, nil;
	} else {
		sqlTemp := fmt.Sprintf(settings.SqlUpdateTemp, "comment", "`name` = ?, `comment` = ?, `book_id` = ?", "`id` = ?");
		stmt, _ := settings.Connection.Prepare(sqlTemp);
		res, _ := stmt.Exec(c.Name, c.Comment, c.BookId, c.Id);
		affected, _ := res.RowsAffected();;
		return affected == 1, nil;
	}
}

//load Comment by ID
func (c Comment) Load(id int64) Comment {
	sqlTemp := fmt.Sprintf(settings.SqlSelectTemp, "*", "comment", "`id` = ?", "");
	row := settings.Connection.QueryRow(sqlTemp, id)
	err := row.Scan(&c.Id, &c.Name, &c.Comment, &c.BookId);
	if err == sql.ErrNoRows {
		return Comment{};
	} else {
		general.CheckError(err);
	}

	return c;
}

//list comments
func (c Comment) List(condition string, page int, limit int) []Comment {
	if (condition == "") {
		condition = "1 = 1";
	}
	limitStr := general.GetLimitStr(page, limit);
	query := fmt.Sprintf(settings.SqlSelectTemp, "*", "comment", condition, limitStr);
	rows, err := settings.Connection.Query(query);
	general.CheckError(err);
	var comments []Comment

	for rows.Next() {
		c := Comment{};
		err := rows.Scan(&c.Id, &c.Name, &c.Comment, &c.BookId)
		general.CheckError(err);
		comments = append(comments, c)
	}

	return comments;
}