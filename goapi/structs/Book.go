package structs

import (
	"fmt"
	"goapi/settings"
	"goapi/general"
	"gopkg.in/validator.v2"
	"errors"
	"database/sql"
)

//book entity struct
type Book struct {
	Id      int64     `json:"id"`
	Title   string    `json:"title",validate:"nonzero"`
	Author  string    `json:"author",validate:"nonzero"`
	Comment []Comment `json:"comment"`
	Cover   string    `json:"-"`
}

//list books
func (b Book) List(condition string, page int, limit int) []Book {
	if (condition == "") {
		condition = "1 = 1";
	}
	limitStr := general.GetLimitStr(page, limit);
	query := fmt.Sprintf(settings.SqlSelectTemp, "*", "book", condition, limitStr);
	rows, err := settings.Connection.Query(query);
	general.CheckError(err);
	var books []Book

	for rows.Next() {
		book := Book{};
		err := rows.Scan(&book.Id, &book.Title, &book.Author, &book.Cover);
		//book.addComments();
		general.CheckError(err);
		books = append(books, book)
	}

	return books;
}

//save book to Database
func (b *Book) Save() (bool, error) {
	if errs := validator.Validate(b); errs != nil {
		return false, errors.New("The book contains inappropriate properties.");
	}
	if (b.Id == 0) {
		sqlTemp := fmt.Sprintf(settings.SqlInsertTemp, "book", "`title`,`author`,`cover`", "?,?,?");
		stmt, _ := settings.Connection.Prepare(sqlTemp);
		res, err := stmt.Exec(b.Title, b.Author, b.Cover);
		general.CheckError(err);
		b.Id, _ = res.LastInsertId();
		b.saveComments();
		return b.Id > 0, nil;
	} else {
		sqlTemp := fmt.Sprintf(settings.SqlUpdateTemp, "book", "`title` = ?, `author` = ?, `cover` = ?", "`id` = ?");
		stmt, _ := settings.Connection.Prepare(sqlTemp);
		_, err := stmt.Exec(b.Title, b.Author, b.Cover, b.Id);
		general.CheckError(err);
		b.saveComments();
		return true,nil;
	}
}

//save all comments passed to Book
func (b *Book) saveComments() bool {
	countSaved := 0;
	for _, Comment := range b.Comment {
		Comment.Id = 0;
		Comment.BookId = b.Id;
		saved, _ := Comment.Save();
		if (saved) {
			countSaved++;
		}
	}

	return countSaved == len(b.Comment);
}

// load book by ID and returns it
func (b Book) Load(id int64) (Book,error) {
	sqlTemp := fmt.Sprintf(settings.SqlSelectTemp, "*", "book", "`id` = ?", "");
	row := settings.Connection.QueryRow(sqlTemp, id)
	err := row.Scan(&b.Id, &b.Title, &b.Author, &b.Cover);
	if err == sql.ErrNoRows {
		return Book{},errors.New("No Entity");
	}
	general.CheckError(err);

	return b,nil;
}

//add comment to book struct
func (b *Book) AddComments() {
	comments := Comment{}.List(fmt.Sprintf("book_id = %v", b.Id), 1, 0);
	b.Comment = comments;
}

//generate test books and add comments to them
func (b Book) GenerateTestBooks() {
	book := Book{Id: 0, Title: "Test Book 1", Author: "Dombi István"};
	book.Save();
	book.addComment("Firstname Lastname", "Comment example");
	book.addComment("Firstname1 Lastname2", "Comment example 2");
	book.addComment("Firstname2 Lastname3", "Comment example 3");
	book = Book{Id: 0, Title: "Test Book 2", Author: "Kovács Dániel Ákos"};
	book.Save();
	book.addComment("Firstname Lastname", "Comment example");
	book.addComment("Firstname1 Lastname2", "Comment example 2");
	book.addComment("Firstname2 Lastname3", "Comment example 3");
	book = Book{Id: 0, Title: "Test Book 3", Author: "Sági Attila"}
	book.Save();
	book.addComment("Firstname Lastname", "Comment example");
	book.addComment("Firstname1 Lastname2", "Comment example 2");
	book.addComment("Firstname2 Lastname3", "Comment example 3");
}

//add comment to book
func (b *Book) addComment(name string, comment string) {
	c := Comment{0, name, comment, b.Id};
	c.Save();
}
