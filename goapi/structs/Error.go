package structs

import (
	"net/http"
	"fmt"
	"os"
	"log"
	"goapi/general"
	"goapi/settings"
)

type Error struct {
	Ip      string
	Error   string
	Request *http.Request
}

func (e Error) Log() error {
	logFile := settings.BasePath + "/error.log";
	if _, err := os.Stat(logFile); err != nil {
		if os.IsNotExist(err) {
			fo, err := os.Create(logFile)
			if err != nil {
				return err
			}
			defer func() {
				fo.Close()
			}();
		}
	}

	f, err := os.OpenFile(logFile, os.O_RDWR | os.O_CREATE | os.O_APPEND, 0666)
	general.CheckError(err);

	log.SetOutput(f);
	log.Println("\r\n");
	log.Println(fmt.Sprintf("REQUEST URL | IP | ERROR: %v %v %v", e.Request.URL, e.Ip, e.Error));

	defer func(){f.Close()}();

	return nil;
}
