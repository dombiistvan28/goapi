package main

import (
	"fmt"
	"net/http"
	"database/sql"

	_ "github.com/go-sql-driver/mysql"
	"github.com/julienschmidt/httprouter"
	"gopkg.in/validator.v2"
	"encoding/json"
	"errors"
	"strconv"
	"io/ioutil"
	"os"
	"io"
	"goapi/structs"
	"goapi/settings"
	"goapi/general"
	"goapi/validation"
	"runtime"
	"path"
)

//authentication middleware if request is not /token
func authMiddleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Content-Type", "application/json");
		if (r.URL.String() != "/token") {
			authRequest := structs.AuthorizedRequest{general.GetHeaderParam(r, "token")};
			if (authRequest.Validate(w)) {
				next.ServeHTTP(w, r);
			}
		} else {
			next.ServeHTTP(w, r);
		}
	})
}

//dbMiddleware middleware if request is not /token
func dbMiddleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		//prepare connection
		prepare();
		next.ServeHTTP(w, r);
	})
}

//main functionality, router paths, processing requests
func main() {
	r := httprouter.New()
	//set custom validators
	validator.SetValidationFunc("validToken", validation.ValidateAccessToken);
	validator.SetValidationFunc("email", validation.ValidateEmail);
	validator.SetValidationFunc("mustContainImage", validation.ValidateMustContainImage);
	validator.SetValidationFunc("imageExtension", validation.ValidateImageExtension);

	setBasePath();

	//requests
	//get token
	r.GET("/token", func(w http.ResponseWriter, r *http.Request, p httprouter.Params) {
		email := general.GetHeaderParam(r, "email");
		password := general.GetHeaderParam(r, "password");
		tokenRequest := structs.TokenRequest{email, password};
		if (tokenRequest.Validate(w)) {
			u := structs.User{}.LoadEmailPass(email, password);
			var err error;
			if (u.Id < 1) {
				err = errors.New("No Entity");
			}

			if (ProcessSuccess(err, r, w, http.StatusBadRequest, "", true)) {
				general.SendResponse(w, http.StatusOK, structs.TokenResponse{u.GetAccessToken()});
				return;
			}
		}
	});

	//get list of books
	r.GET("/book", func(w http.ResponseWriter, r *http.Request, p httprouter.Params) {
		page := general.GetUrlParamInt(r, settings.LimitParam);
		general.SendResponse(w, http.StatusOK, structs.BookListResponse{structs.Book{}.List("", page, settings.EntityLimit)});
	});

	//get book
	r.GET("/book/:book_id", func(w http.ResponseWriter, r *http.Request, p httprouter.Params) {
		bookId, _ := strconv.ParseInt(p.ByName("book_id"), 10, 64);
		ber := structs.GetBookEntityRequest{bookId};
		if (ber.Validate(w)) {
			Book, err := structs.Book{}.Load(bookId);
			if (ProcessSuccess(err, r, w, http.StatusNotFound, "", true)) {
				Book.AddComments();
				general.SendResponse(w, http.StatusOK, structs.BookEntityResponse{Book: Book});
			}

			return;
		}
	});

	//insert book
	r.PUT("/book", func(w http.ResponseWriter, r *http.Request, p httprouter.Params) {
		data, _ := ioutil.ReadAll(r.Body);
		iber := structs.InsertBookEntityRequest{}
		err := json.Unmarshal(data, &iber)

		if (ProcessSuccess(err, r, w, http.StatusBadRequest, "Format Error", false)) {
			if (iber.Validate(w)) {
				Book := structs.Book{0, iber.Title, iber.Author, iber.Comment, ""};
				_, err := Book.Save();
				if (ProcessSuccess(err, r, w, http.StatusInternalServerError, "Processing error", false)) {
					general.SendResponse(w, http.StatusCreated, structs.SuccessModifyResponse{Book});
				}
			}
		}
	});

	//update book
	r.POST("/book/:book_id", func(w http.ResponseWriter, r *http.Request, p httprouter.Params) {
		bookId, _ := strconv.ParseInt(p.ByName("book_id"), 10, 64);
		data, _ := ioutil.ReadAll(r.Body);
		uber := structs.UpdateBookEntityRequest{}
		err := json.Unmarshal(data, &uber)
		uber.Id = bookId;
		if (ProcessSuccess(err, r, w, http.StatusBadRequest, "Format Error", false) && uber.Validate(w)) {
			Book, err := structs.Book{}.Load(uber.Id);
			if (ProcessSuccess(err, r, w, http.StatusBadRequest, "", true)) {
				Book.Title = uber.Title;
				Book.Author = uber.Author;
				Book.Comment = uber.Comment;
				_, err := Book.Save();
				ProcessSuccess(err, r, w, http.StatusInternalServerError, "Processing error", false);
				Book.AddComments();
				general.SendResponse(w, http.StatusOK, structs.SuccessModifyResponse{Book});
			}
		}
	});

	//insert book cover
	r.PUT("/book/:book_id/cover", func(w http.ResponseWriter, r *http.Request, p httprouter.Params) {
		bookId, _ := strconv.ParseInt(p.ByName("book_id"), 10, 64);
		Book, err := structs.Book{}.Load(bookId);
		ibcr := structs.InsertBookCoverRequest{}
		if (ProcessSuccess(err, r, w, http.StatusNotFound, "", true) && ibcr.Validate(w)) {
			image := structs.Image{"file", nil, "", 0, "", "", nil}
			_, err := image.Init(r);
			if (ProcessSuccess(err, r, w, http.StatusInternalServerError, "Processing Error", false)) {
				err := validator.Validate(image);
				if (ProcessSuccess(err, r, w, http.StatusBadRequest, "", true)) {
					var uploaded bool;
					uploaded, Book.Cover, err = image.Upload(r, fmt.Sprintf("/book/%v", bookId), "");
					if (ProcessSuccess(err, r, w, http.StatusInternalServerError, "Processing Error", false) && uploaded) {
						_, err = Book.Save();
						Book.AddComments();
						if (ProcessSuccess(err, r, w, http.StatusInternalServerError, "Processing Error", false)) {
							general.SendResponse(w, http.StatusCreated, structs.SuccessModifyResponse{Book});
						}
					}
				}
			}
		}
	});

	//get book cover image
	r.GET("/book/:book_id/cover", func(w http.ResponseWriter, r *http.Request, p httprouter.Params) {
		bookId, _ := strconv.ParseInt(p.ByName("book_id"), 10, 64);
		gbcr := structs.GetBookCoverRequest{}
		if (gbcr.Validate(w)) {
			Book, err := structs.Book{}.Load(bookId);
			if (ProcessSuccess(err, r, w, http.StatusBadRequest, "", true)) {
				if (Book.Cover == "") {
					ProcessSuccess(errors.New("No Cover Image"), r, w, http.StatusNoContent, "", true);
				} else {
					img, Err := os.Open(settings.BasePath + "/" + Book.Cover);
					buffer := make([]byte, 512)
					_, Err = img.Read(buffer)
					ProcessSuccess(Err, r, w, http.StatusInternalServerError, "Processing Error", false);
					img.Seek(0, 0)
					contentType := http.DetectContentType(buffer)

					defer img.Close()
					w.Header().Set("Content-Type", contentType)
					io.Copy(w, img)
				}
			}
		}
	});

	// Fire up the server
	http.ListenAndServe("localhost:8090", dbMiddleware(authMiddleware(r)));
}

// set connection
func setConnection(host string, port string, user string, pass string, dbname string, charset string) {
	var err error;
	settings.Connection, err = sql.Open(settings.DbDriver, fmt.Sprintf("%s:%s@tcp(%s:%s)/%s?charset=%s", user, pass, host, port, dbname, charset));
	general.CheckError(err);
}



//connect to database if it can, or call creating database and tables
func prepareConnection() {
	var err error;
	setConnection(settings.JsonDbConf.DbHost, settings.JsonDbConf.DbPort, settings.JsonDbConf.DbUser, settings.JsonDbConf.DbPass, settings.JsonDbConf.DbName, settings.JsonDbConf.CharSet);
	err = settings.Connection.Ping();
	if err != nil {
		createStructure();
	}
}

//create database structures, and call creating test user and books with their comments
func createStructure() {
	setConnection(settings.JsonDbConf.DbHost, settings.JsonDbConf.DbPort, settings.JsonDbConf.DbUser, settings.JsonDbConf.DbPass, "", settings.JsonDbConf.CharSet);
	general.ExecSql(fmt.Sprintf("CREATE DATABASE IF NOT EXISTS `%s` DEFAULT CHARACTER SET %s COLLATE %s;", settings.JsonDbConf.DbName, settings.JsonDbConf.CharSet, settings.JsonDbConf.DbCollate));
	setConnection(settings.JsonDbConf.DbHost, settings.JsonDbConf.DbPort, settings.JsonDbConf.DbUser, settings.JsonDbConf.DbPass, settings.JsonDbConf.DbName, settings.JsonDbConf.CharSet);
	general.ExecSql(fmt.Sprintf("CREATE TABLE IF NOT EXISTS `%s`.`user` ( `id` INT(10) NOT NULL AUTO_INCREMENT , `email` VARCHAR(255) NOT NULL , `password` VARCHAR(32) NOT NULL , `token` VARCHAR(255) NOT NULL , `token_expires_at` TIMESTAMP NOT NULL , PRIMARY KEY (`id`), UNIQUE (`email`)) ENGINE = InnoDB;", settings.JsonDbConf.DbName));
	general.ExecSql(fmt.Sprintf("CREATE TABLE IF NOT EXISTS `%s`.`book` ( `id` INT(10) NOT NULL AUTO_INCREMENT , `title` VARCHAR(255) NOT NULL , `author` VARCHAR(255) NOT NULL, `cover` VARCHAR(255) NOT NULL , PRIMARY KEY (`id`)) ENGINE = InnoDB; ", settings.JsonDbConf.DbName));
	general.ExecSql(fmt.Sprintf("CREATE TABLE `%s`.`comment` ( `id` INT(10) NOT NULL AUTO_INCREMENT , `name` VARCHAR(255) NOT NULL , `comment` TEXT NOT NULL , `book_id` INT(10) NOT NULL , PRIMARY KEY (`id`)) ENGINE = InnoDB;", settings.JsonDbConf.DbName));
	general.ExecSql(fmt.Sprintf("ALTER TABLE `%s`.`comment` add constraint FK_COMMENT_BOOK_ID_BOOK_ID FOREIGN KEY ( book_id ) REFERENCES `%s`.`book`(id);", settings.JsonDbConf.DbName, settings.JsonDbConf.DbName));
	structs.User{}.GenerateTestUser();
	structs.Book{}.GenerateTestBooks();
}

//log error and exit running
func ProcessSuccess(err error, r *http.Request, w http.ResponseWriter, code int, message string, canUseError bool) bool {
	if err != nil {
		Error := structs.Error{r.RemoteAddr, err.Error(), r};
		Error.Log();
		var passMessage string;
		if (canUseError) {
			passMessage = err.Error();
		} else {
			passMessage = message;
		}
		general.SendResponse(w, code, structs.ErrorResponse{passMessage});
		return false;
	}

	return true;
}

//set base path of script
func setBasePath() {
	_, filename, _, ok := runtime.Caller(0);
	if ok {
		settings.BasePath = path.Dir(filename);
	}
}
//load json config to connect to database
func loadJsonDBConfig(){
	file, _ := ioutil.ReadFile(settings.BasePath + "/db.json");
	json.Unmarshal(file, &settings.JsonDbConf)
}

//prepare connection, etc
func prepare(){
	loadJsonDBConfig();
	prepareConnection();
}
