package validation

import (
	"errors"
	"fmt"
	"strings"
	"reflect"
	"gopkg.in/validator.v2"
	"regexp"
	"github.com/dgrijalva/jwt-go"
	"goapi/settings"
)

// validate access token
func ValidateAccessToken(v interface{}, param string) error {
	st := reflect.ValueOf(v)
	if st.Kind() != reflect.String {
		return validator.ErrUnsupported
	}

	token, err := jwt.Parse(st.String(), func(token *jwt.Token) (interface{}, error) {
		// Don't forget to validate the alg is what you expect:
		if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
			return nil, fmt.Errorf("Unexpected signing method: %v", token.Header["alg"])
		}

		return []byte(settings.Secret), nil
	})

	if (err != nil) {
		return errors.New(settings.AuthenticationError);
	}

	_, ok := token.Claims.(jwt.MapClaims);
	if !ok || !token.Valid {
		return errors.New(settings.AuthenticationError)
	}

	return nil;
};

//mime type must contain image
func ValidateMustContainImage(v interface{}, param string) error {
	st := reflect.ValueOf(v)
	if st.Kind() != reflect.String {
		return validator.ErrUnsupported
	}

	if (!strings.Contains(st.String(), "image")) {
		return errors.New("The file must be an image.")
	}

	return nil
}

//image extensions validation
func ValidateImageExtension(v interface{}, param string) error {
	st := reflect.ValueOf(v)
	if st.Kind() != reflect.String {
		return validator.ErrUnsupported
	}

	allowedExts := []string{"jpe", "jpg", "jpeg", "png", "bmp", "wbmp", "tiff", "tif"};
	allowed := false;
	for _, part := range allowedExts {
		if (st.String() == part) {
			allowed = true;
			break;
		}
	}

	if (!allowed) {
		return errors.New(fmt.Sprintf("The file extension is not allowed. Allowed extensions are: %s.", strings.Join(allowedExts, ", ")));
	}

	return nil
}

//validate email
func ValidateEmail(v interface{}, param string) error {
	st := reflect.ValueOf(v)
	if st.Kind() != reflect.String {
		return validator.ErrUnsupported
	}

	Re := regexp.MustCompile(`^[a-z0-9._%+\-]+@[a-z0-9.\-]+\.[a-z]{2,4}$`)

	if (!Re.MatchString(st.String())) {
		return errors.New("The format of the email address is not valid.")
	}

	return nil
}
